﻿using System;
using Mesozoic;

namespace Laboratory
{
    public class Laboratory
    {
        public static TDinosaur CreateDinosaur<TDinosaur>(string name, int age = 0) where TDinosaur : Dinosaur
        {
            //Do something magic here
            return (TDinosaur)Activator.CreateInstance(typeof(TDinosaur), name, age);
        }
    }
}