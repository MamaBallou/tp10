﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;
using Laboratory;

namespace MesozoicSolution
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dinosaur nessie = Laboratory.Laboratory.CreateDinosaur<Diplodocus>("Nessie", 11);
            Dinosaur louis = Laboratory.Laboratory.CreateDinosaur<TyrannosaurusRex>("Louis", 12);
            Console.WriteLine(louis.hug(nessie));
            Console.ReadKey();
        }
    }
}
