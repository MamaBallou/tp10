﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mesozoic
{
    public class Stegausaurus : Dinosaur
    {
        static Stegausaurus()
        {
            Stegausaurus.specie = "Stegausaurus";
        }
        protected override string Specie { get { return "Stegausaurus"; } }
        public Stegausaurus(string name, int age) : base(name, age)
        {

        }
        public override string roar()
        {
            return "Braa";
        }
        public override string sayHello()
        {
            return string.Format("{0}, {1}, {2} ans, célibataire.", this.name, Stegausaurus.specie, this.age);
        }
    }
}
