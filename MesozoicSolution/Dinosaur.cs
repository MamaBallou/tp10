﻿using System;

namespace Mesozoic
{
    public abstract class Dinosaur
    {
        public string name;
        public static string specie;
        public int age;

        protected virtual string Specie { get { return "Dinosaur"; } }

        public Dinosaur(string name,  int age)
        {
            this.name = name;
            this.age = age;
        }

        public virtual string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, Dinosaur.specie, this.age);
        }

        public virtual string roar()
        {
            return "Grrr";
        }

        public string getName()
        {
            return this.name;
        }

        public string getSpecie()
        {
            return this.Specie;
        }

        public int getAge()
        {
            return this.age;
        }

        public void setName()
        {
            Console.Write("Donner son nom : ");
            string nom = Console.ReadLine();
            this.name = nom;
        }

        public void setSpecie()
        {
            Console.Write("Donner son espece : ");
            string specie = Console.ReadLine();
            Dinosaur.specie = specie;
        }

        public void setAge()
        {
            int age;
            string age_string;
            do
            {
                Console.Write("Donner son age : ");
                age_string = Console.ReadLine();
            } while (!int.TryParse(age_string, out age));
            this.age = age;
        }

        public string hug(Dinosaur dinosaur)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}.", this.name, dinosaur.name);
        }
    }
}