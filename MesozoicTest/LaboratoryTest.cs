﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;
using Laboratory;

namespace MesozoicTest
{
    [TestClass]
    public class LaboratoryTest
    {
        [TestMethod]
        public void TestCreateDinosaur()
        {
            Dinosaur louis = Laboratory.Laboratory.CreateDinosaur<Stegausaurus>("Louis", 12);
            Dinosaur louis2 = new Stegausaurus("Louis", 12);
            Assert.AreEqual(louis.name, louis2.name);
            Assert.AreEqual(louis.getSpecie(), louis2.getSpecie());
            Assert.AreEqual(louis.age, louis2.age);
        }
    }
}
